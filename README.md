REXPaint Rust crate
===================

[![Crates.io](https://img.shields.io/crates/v/rexpaint.svg)](https://crates.io/crates/rexpaint)

![REXPaint logo](/screenshot/rexpaint_logo.png)

This crate provides functionality for reading and writing `.xp` files of the
Grid Sage Games [REXPaint](https://www.gridsagegames.com/rexpaint/)
ASCII art editor.

This can be used to import the images directly into a game, or to build  tools
for generating or manipulating images.

Documentation
-------------

Automatically generated documentation for the crate can be found on [docs.rs](https://docs.rs/rexpaint/0.1.0/rexpaint/).

Examples
--------

A few example tools are part of the crate 

- [noise](examples/noise.rs) generates turbulence noise, puts it the background of character cells, and writes to `noise.xp`,
  which can then be read into REXPaint. The color map used to map noise values to colors is selectable.

![Running noise example](/screenshot/noise_example.png)

- [view](examples/view.rs) prints rexpaint images to the terminal (requires true-color support). This assumes that
  a codepage 437 font was used.

![Running view example](/screenshot/rexpaint_ex.png)

- [textwrap](examples/textwrap.rs) wraps text into a `.xp` file of a specified size like
```bash
cargo run --example textwrap /path/to/text test.xp 16 8
```
The text is converted from unicode to codepage 437, undefined characters are replaced with `■`!

- [tcod](examples/tcod.rs) displays the `xp` image given on the command line using the popular tile-based rendering library `libtcod`
([tcod-rs](https://github.com/tomassedovic/tcod-rs)). This is likely the example that you want if you want to use it in a roguelike game.

See these examples for how to use the library.
