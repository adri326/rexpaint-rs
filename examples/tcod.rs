//! `tcod` shows a rexpaint image using libtcod console
extern crate rexpaint;
extern crate tcod;

use rexpaint::{XpColor, XpFile};
use std::fs::File;
use std::{char, env};
use tcod::colors::Color;
use tcod::console::{blit, Console, FontLayout, FontType, Offscreen, Root};

const SCREEN_WIDTH: i32 = 80;
const SCREEN_HEIGHT: i32 = 50;

/// Convert rexpaint color to libtcod color
pub fn color_to_tcod(color: XpColor) -> Color {
    Color {
        r: color.r,
        g: color.g,
        b: color.b,
    }
}

/// Draw a XpFile to a libtcod console, layer by layer, cell by cell.
/// As an optimization, if this is to be done every frame, it is better to do this once to a
/// console, then blit from there.
pub fn draw_xp<T: Console>(con: &mut T, bx: i32, by: i32, xp: &XpFile) {
    for layer in &xp.layers {
        for y in 0..layer.height {
            for x in 0..layer.width {
                let cell = &layer.get(x, y).unwrap();
                con.put_char_ex(
                    bx + x as i32,
                    by + y as i32,
                    char::from_u32(cell.ch).unwrap(),
                    color_to_tcod(cell.fg),
                    color_to_tcod(cell.bg),
                );
            }
        }
    }
}

/// Convert a XpFile to a Offscreen console
pub fn xp_to_offscreen(xp: &XpFile) -> Offscreen {
    assert!(!xp.layers.is_empty());
    let mut ret = Offscreen::new(xp.layers[0].width as i32, xp.layers[0].height as i32);
    draw_xp(&mut ret, 0, 0, xp);
    ret
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut f = File::open(&args[1]).unwrap();
    let xp = XpFile::read(&mut f).unwrap();
    let mut root = Root::initializer()
        .font("test_images/cp437_14x14.png", FontLayout::AsciiInRow)
        .font_type(FontType::Greyscale)
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title(&args[1])
        .init();

    let con = xp_to_offscreen(&xp);
    blit(&con, (0, 0), (0, 0), &mut root, (0, 0), 1.0, 1.0);
    root.flush();
    root.wait_for_keypress(true);
}
