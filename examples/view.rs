//! `view` prints rexpaint images to the console (requires true-color support in the terminal). This assumes that
//! the codepage 437 character set was used.
extern crate codepage_437;
extern crate rexpaint;

use codepage_437::CP437_WINGDINGS;
use rexpaint::XpFile;
use std::env;
use std::fs::File;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut f = File::open(&args[1]).unwrap();
    let xp = XpFile::read(&mut f).unwrap();

    for layer in &xp.layers {
        for y in 0..layer.height {
            for x in 0..layer.width {
                let cell = layer.get(x, y).unwrap();
                let ch = if cell.ch != 0 {
                    CP437_WINGDINGS.decode(cell.ch as u8)
                } else {
                    ' '
                };
                if cell.bg.is_transparent() {
                    print!(
                        "\x1b[38;2;{};{};{}m{}\x1b[0m",
                        cell.fg.r, cell.fg.g, cell.fg.b, ch,
                    );
                } else {
                    print!(
                        "\x1b[38;2;{};{};{};48;2;{};{};{}m{}\x1b[0m",
                        cell.fg.r, cell.fg.g, cell.fg.b, cell.bg.r, cell.bg.g, cell.bg.b, ch,
                    );
                }
            }
            println!();
        }
    }
}
